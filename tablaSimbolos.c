#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "tablaSimbolos.h"
#include "funciones.h"
#include "bison.tab.h"

static hashtable_t *tablaSimbolos;

void inicializarTablaSimbolos() {

    tablaSimbolos = ht_create(128);

	int i;
	entry_t *ptr;

	for (i = 0; arith_fncts[i].fname != 0; i++) {
		
		data_t t;
	
		t.type = FNCT;

		ptr = insertarTablaSimbolos (arith_fncts[i].fname, t);

		ptr->content.value.fnctptr = arith_fncts[i].fnct;
	}

	for (i = 0; constantes[i].fname != 0; i++) {
		data_t t;
	
		t.type = CONST;

		ptr = insertarTablaSimbolos (constantes[i].fname, t);

		ptr->content.value.var = constantes[i].valor;
	}
}

entry_t *insertarTablaSimbolos(char *identificador, data_t content) {

    return ht_set(tablaSimbolos, identificador, content);
}

entry_t *buscarTablaSimbolos(char *identificador) {

    return ht_get(tablaSimbolos, identificador);

}

void imprimirTablaSimbolos() {

    ht_print(tablaSimbolos);
}

void borrarTablaSimbolos() {
    ht_free(tablaSimbolos);
}


