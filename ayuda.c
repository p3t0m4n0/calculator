#include <stdio.h>
#include <stdlib.h>

void ayuda() {
	printf("Calculadora por línea de comandos\n");
	printf("\t* Modo interactivo. Permite la introducción de comandos que ejecuten operaciones por parte del usuario. Ejecucion: ./calculator\n");
	printf("\n");
	printf("\t* Modo lectura desde fichero. Permite la ejecución de operaciones matemáticas contenidas en un fichero. Ejecucion: './calculator [nombre de archivo]'\n");
	printf("\n");
	printf("\t* Muestra al usuario una pequeña descripción del programa y sus modos de funcionamiento. Ejecucion:'./calculator -h ó --help'\n");
	printf("\n");

}
