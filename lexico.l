%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tablaSimbolos.h"
#include "math.h"
#include "bison.tab.h"
%}

/* Definiciones */

NUMERO	[[:digit:]]+|[[:digit:]]+"."[[:digit:]]*
IDENTIFICADOR	[[:alpha:]][[:alnum:]]*
EXIT	quit|exit
PRINT	print|show

%%

{EXIT}				return EXIT;

{PRINT}				return PRINT;

{NUMERO}			{	
						yylval.val = atof(yytext);

						return NUM;
					}

{IDENTIFICADOR}		{	
				entry_t *s = buscarTablaSimbolos(yytext);
					if(s == 0){
						data_t t;
						t.type = VAR;
						t.iniciada = 0;
						t.value.var = 0;
						s = insertarTablaSimbolos(yytext,t);
					}
					yylval.tptr = s;		
					return s->content.type;
					}

"\n"				return '\n';

";"					return ';';

[-|=|+|*|/|^|(|)]	return (int) yytext[0];


[ \t]+ 				

<<EOF>>     		return 0;

%%

int yywrap() {
	return 1;
}











