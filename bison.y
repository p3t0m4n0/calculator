%{
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ayuda.h"
#include "tablaSimbolos.h"

extern int yylex();
extern int yyparse();
extern FILE *yyin;
	
void yyerror(const char *s);

%}
%union {
double val; 
entry_t *tptr; 
}
%token <val> NUM
%token <tptr> VAR FNCT CONST
%type <val> exp
%token <val> EXIT
%token <val> PRINT

%right '='
%left '-' '+'
%left '*' '/'
%left NEG 
%right '^' 
/* Gramática */
%%
input:	/* cadena vacıa */
	| input line
;
line:	'\n'		{printf(">");}
	| exp '\n'	{ printf("\t%.10g\n",$1); printf("\n>");	}
	| exp ';' '\n' { printf("\n>"); }
	| error '\n'	{ yyerrok; printf(">");	}
	| EXIT '\n'	{ exit(0);	}
	| PRINT '\n' { imprimirTablaSimbolos(); }
;
exp:	NUM	{ $$ = $1;	}
	| VAR	{ 
				if($1->content.iniciada == 1) {
					$$ = $1->content.value.var;	
				} else {
					yyerror("error, variable no iniciada");
					YYERROR;
				}
				
			}
	| VAR '=' exp	{ 
				$$ = $3; 
				$1->content.value.var= $3;
				$1->content.iniciada = 1;
			}
	| FNCT '(' exp ')'	{ 
					if(strcmp($1->key, "sqrt") == 0 && $3 < 0) {
						yyerror("error, numero negativo");
						YYERROR;
					} else {
						$$ = (*($1->content.value.fnctptr))($3); 
					}
				}
	| exp '+' exp	{ $$ = $1 + $3;	}
	| exp '-' exp	{ $$ = $1 - $3;	}
	| exp '*' exp	{ $$ = $1 * $3;	}
	| exp '/' exp	{
				if($3 == 0) {
					yyerror("error, division por cero");
					YYERROR;
				} else {
					$$ = $1 / $3;
				}	
			}
	| '-' exp %prec	NEG { $$ = -$2;	}
	| exp '^' exp	{ $$ = pow ($1,$3);	}
	| '(' exp ')'	{ $$ = $2;	}
	| CONST {$$ = $1->content.value.var;}
;
/* Fin de la gramatica*/
%%

int main(int argc, char **argv) {
	/*Se crea la tabla*/
    inicializarTablaSimbolos();
    if (argc == 2) {
    	if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
    		ayuda();
    		exit(0);
    	} else {
    		FILE *fp = fopen(argv[1], "rt");
    		if (fp == NULL) {
    			printf("Archivo no encontrado");
    		} else {
    			printf("Cargando archivo %s", argv[1]);
    			yyset_in(fp);
    		}
    	}
    	
    } else {
    	printf("Ejecución interactiva\n");
		printf(">");
    }
	yyparse();
	
	return(EXIT_SUCCESS);
}

 void yyerror (const char *s) {
   printf ("ERROR: %s\n", s);
 }









































