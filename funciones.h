#include <math.h>

struct init {
	char *fname;
	double (*fnct)();
};

struct init arith_fncts[]= {
	"sin", sin,
	"cos", cos,
	"atan", atan,
	"ln", log,
	"exp", exp,
	"sqrt", sqrt,
	0, 0
};

struct initConst {
	char *fname;
	double valor;
};

struct initConst constantes[]= {
	"pi", 3.141592,
	"e", 2.71828
};

