#include "tablaHash.h"

void inicializarTablaSimbolos();
entry_t *insertarTablaSimbolos(char *identificador, data_t content);
entry_t *buscarTablaSimbolos(char *identificador);
void imprimirTablaSimbolos();
void borrarTablaSimbolos();
