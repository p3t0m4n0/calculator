struct data_s{
    int type;
    int iniciada;
    union{
      double var;
      double (*fnctptr)();
    }value;
};

typedef struct data_s data_t;

struct entry_s {
    char *key;
    data_t content;
    struct entry_s *next;
};

typedef struct entry_s entry_t;

struct hashtable_s {
    int size;
    struct entry_s **table;
};

typedef struct hashtable_s hashtable_t;

/* Create a new hashtable. */
hashtable_t *ht_create(int size);

/* Hash a string for a particular hash table. */
int ht_hash(hashtable_t *hashtable, char *key);

/* Create a key-value pair. */
entry_t *ht_newpair(char *key, data_t content);

/* Insert a key-value pair into a hash table. */
entry_t *ht_set(hashtable_t *hashtable, char *key, data_t content);

/* Retrieve a key-value pair from a hash table. */
entry_t *ht_get(hashtable_t *hashtable, char *key);

/* Print all elements of the hash table*/
void ht_print(hashtable_t *hashtable);

/* Free memory*/
void ht_free(hashtable_t *hashtable);
